# Linux_Crashcourse11_Bash.txt

Bash Scripting

Puts bash commands into a file and allows you to run a complex series of commands over and over.

The Shebang ( or Hash-Bang )
#! - an interpreter directive, it indicates that the following file is to be run through the inticated interpreter, such as bash, python, or perl.

Scripts are just text files.  In Windows and other operating systems the file extension ( .sh .py .pl etc ) lets the operating system know how to handle the file, but not in linux.  One must set the Sheband correctly.

The Sheband tells the operating system which interpreted to run the script file through. Must include the absolute path to the interpreter.


Examples:

    Bash		#!/usr/bin/bash
    Python		#!/usr/bin/python  
    Perl		#!/usr/bin/perl
    Shell       #!/usr/bin/sh  # see link at bottom for differences between sh and bash

There are many others

It can include parameters

#!/usr/bin/perl -T 
Execute using Perl with the option for taint checks

This can also be made more portability across different unix and linux operating systems with the env command.  This looks up the absolute path on a given operating system.

#!/usr/bin/env bash
#!/usr/bin/env python

https://en.wikipedia.org/wiki/Shebang_(Unix)

Its best to give scripts executable permissions so that they will run when their name is used
A simple bash script 

Create a script called hello.sh with the following:

#!/usr/bin/env bash
echo "Hello World!"

Try to run it:

./hello.sh

Now try to run it with:
bash ./hello.sh

Now give it executable permissions:

chmod 700 hello.sh

or

chmod u+x hello.sh

Now try to run it.

Bash variables 

Bash scripts frequently use variables to simply scripts and make it easier to change.

[ show example ]

Variables can also be the output of commands:

Create a file called date.sh with the following:

#!/bin/env bash
DATE=$(date +%F)
echo $DATE

In older scripts it may use backticks for the the same purpose.  This is now deprecated
DATE=`date +%F`

echo vs print
[add more here]
Special Parameters: $0  $1  $2  $# $?

$0 is the script itself

$1 is the first argument to the script

$2 is the 2nd argument, etc

$# is the number of arguments passed.

$? - The exit status of the most recent command 

There are more. Also see:  https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html

Create this script: arguments.sh

#!/bin/env bash
echo "Script is: $0"
echo "Number of arguments are: $#"
echo "First argument is: $1"
echo "2nd argument is: $2"

./arguments.sh
./arguments.sh Hello
./arguments.sh Hello World


http://tldp.org/LDP/abs/html/othertypesv.html

Conditionals:

If-then structures in bash take the form of:

if [ <condition> ]
then
   <something>
else
   <something_else>
fi

Notice where the line breaks are. The above can also be written as:

if [ <condition> ]; then
	<something>
else
	<something_else>
fi

OR

if [ <condition> ]; then <something>; else <something_else>; fi

Typical conditions:

T1="foo"
T2="bar"
if [ "$T1" = "$T2" ]; then
   echo expression evaluated as true
else
   echo expression evaluated as false
fi

Note there is a space before and after =

http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-6.html


 AND

if [ -n $1 ] && [ -r $1 ]
then
  echo "File exists and is readable"
fi

http://bencane.com/2014/01/27/8-examples-of-bash-if-statements-to-get-you-started/

Conditionals also can be used to do certain file tests:

One of the most useful is to test if a file exists:
Example 1:
if [ -e <file> ]; then <something>; fi

Example 2:
FILE=~/.bashrc
if [ -e $FILE ]; then tail $FILE; fi

Example 3:
if [ ! e $FILE ]; then exit; fi

Test if a file is a regular file ( and not a directory or other file type )

if [ -f $FILE ]; then tail $FILE; fi

A file is of non-zero size:

if [ -s $FILE ]; then tail $FILE; fi

http://www.tldp.org/LDP/abs/html/fto.html

Note:
There are also double brackets for conditions [[ <condition> ]] this is called the extended test command

There is also the double parentheses  (( <condition )) which expands an arithmetic expression.

http://tldp.org/LDP/abs/html/testconstructs.html#DBLBRACKETS 

Some special properties of loops:

Creating a loop with sequential iteration:

for i in {1..25}; do echo $i; done

also letters

for i in {a..z}; do echo $i; done

http://www.cyberciti.biz/faq/bash-for-loop/


Bash allows you to process all files in a directory through a loop:

#!/bin/env bash
FILES=/path/to/*
for f in $FILES
do
  	echo "Processing $f file..."
  	# take action on each file. $f store current file name
  	cat $f
done

http://www.cyberciti.biz/faq/bash-loop-over-file/
  
Exception Handling in bash: https://stackoverflow.com/questions/22009364/is-there-a-try-catch-command-in-bash


Where to find more bash help

Bash Programming HOWTO
http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html

Bash Guide for Beginners
http://www.tldp.org/LDP/Bash-Beginners-Guide/html/

Lhunath's Bash Guide
http://mywiki.wooledge.org/BashGuide

Advanced bash scripting guide
http://tldp.org/LDP/abs/html/

Goalkicker Bash Book
http://goalkicker.com/BashBook/ 

Bash Scripting Tutorial
https://ryanstutorials.net/bash-scripting-tutorial/

Note: bash has been around since 1989 and has changed over the years. When searching for help online, look for the latest solution to a problem.

Bash is a superset of  sh the POSIX standard shell command language. Although they are similar, there are differences between what sh can do and what bash can do.
https://stackoverflow.com/questions/5725296/difference-between-sh-and-bash

Bash coding style guide
https://github.com/icy/bash-coding-style

